extends KinematicBody2D

const GRAVITY = 500.0
const SCREEN_BOUNDARIES = 35
var velocity = Vector2()
export (int) var speed
export (int) var jump_speed
var screensize

var default_texture = preload("res://assets/pepe_sad.png")
var crying_texture = preload("res://assets/pepe_cry.png")

# Called when the node enters the scene tree for the first time.
func _ready():
	screensize = get_viewport_rect().size

func cry():
	$Sprite.set_texture(crying_texture)
	$Timer.start()

func _on_Timer_timeout():
	$Sprite.set_texture(default_texture)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	move_and_slide(velocity, Vector2(0, -1))

	position.x = clamp(position.x, 0 + SCREEN_BOUNDARIES, screensize.x - SCREEN_BOUNDARIES);
	position.y = clamp(position.y, 0 + SCREEN_BOUNDARIES, screensize.y - SCREEN_BOUNDARIES);

func get_input():
	velocity.x = 0

	if Input.is_action_pressed("ui_right"):
		velocity.x += speed
	if Input.is_action_pressed("ui_left"):
		velocity.x -= speed
	if Input.is_action_just_pressed("jump"):
		velocity.y = jump_speed


func _physics_process(delta):
	velocity.y +=  delta * GRAVITY
	get_input()
#	velocity = move_and_slide(velocity, Vector2(0, -1))
	
	var motion = velocity * delta
	move_and_collide(motion)
