extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func spawn_burning_tile():
	var tile_id = "BurningTiles/" + ((randi() % 21) + 1) as String
	var tile_to_spawn = get_node(tile_id)
	tile_to_spawn.activate()
	print(tile_id)
#	print(tile_to_spawn)


func _on_BurningTileTimer_timeout():
	spawn_burning_tile()
	pass # Replace with function body.
