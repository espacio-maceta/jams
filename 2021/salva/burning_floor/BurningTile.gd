extends Area2D

var is_burning
var hot_tile_texture = preload("res://assets/hot_tile.png")
var burning_tile_texture = preload("res://assets/burning_tile.png")
var burnt

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	
func activate():
	visible = true
	is_burning = false
	burnt = false
	$BurnTimer.start(1)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Area2D_body_entered(body):
	var Player = get_tree().get_root().get_node("Game/Player")
#	!body.is_in_group('Floor')
	if (is_burning && !burnt):
		Player.cry()
		burnt = true
		print("BURN!")

func _on_BurnTimer_timeout():
	is_burning = true
	$CollisionShape2D.disabled = false
	$Sprite.set_texture(burning_tile_texture)
	$BackToNormalTimer.start(2)

func _on_BackToNormalTimer_timeout():
	set_normal_tile()

func set_normal_tile():
	$Sprite.set_texture(hot_tile_texture)
	is_burning = false
	$CollisionShape2D.disabled = true
	visible = false
