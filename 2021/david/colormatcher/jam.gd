extends Node2D

var rng = RandomNumberGenerator.new()
var destination_r = 1
var destination_g = 1
var destination_b = 1
var controled_r = 1
var controled_g = 1
var controled_b = 1
var points = 0
export var INCREMENT = 0.01
export var DIFFICULTY_TRESHOLD = 2

func _ready():
	rng.randomize()
	set_random_color()
	set_controled_color()
	$Points.change_color(Color(0,0,0,1), points)

func _process(_delta):
	var has_changed = false
	if Input.is_action_just_pressed("ui_insert"):
		controled_r += INCREMENT
		if controled_r > 1:
			controled_r = 0
		has_changed = true
	if Input.is_action_just_pressed("ui_supr"):
		controled_r -= INCREMENT
		if controled_r < 0:
			controled_r = 1
		has_changed = true
	if Input.is_action_just_pressed("ui_home"):
		controled_g += INCREMENT
		if controled_g > 1:
			controled_g = 0
		has_changed = true
	if Input.is_action_just_pressed("ui_end"):
		controled_g -= INCREMENT
		if controled_g < 0:
			controled_g = 1
		has_changed = true
	if Input.is_action_just_pressed("ui_page_up"):
		controled_b += INCREMENT
		if controled_b > 1:
			controled_b = 0
		has_changed = true
	if Input.is_action_just_pressed("ui_page_down"):
		controled_b -= INCREMENT
		if controled_b < 0:
			controled_b = 1
		has_changed = true
	if Input.is_action_just_pressed("ui_select"):
		controled_r = 1
		controled_g = 1
		controled_b = 1
		has_changed = true
	set_controled_color()
	
	if player_has_won():
		set_random_color()
		increment_points()
	if has_changed:
#		print([destination_r, destination_g, destination_b])
		print([controled_r, controled_g, controled_b])
#		print('===========')

func set_random_color():
	destination_r = get_random_float()
	destination_g = get_random_float()
	destination_b = get_random_float()
	var destination = Color(destination_r,destination_g,destination_b, 1)
	var oposite = Color(get_oposite_float(destination_r), get_oposite_float(destination_g), get_oposite_float(destination_b),1)
	$ColorDestination.color = destination
	$Instructions.change_color(oposite)

func set_controled_color():
	var current = Color(controled_r,controled_g,controled_b, 1)
	var oposite = Color(get_oposite_float(controled_r), get_oposite_float(controled_g), get_oposite_float(controled_b),1)
	$Points.change_color(oposite, points)
	$ColorControled.color = current

func get_random_float():
	var number = rng.randf_range(0.00, 1.00)
	return stepify(number, 0.01)
	
func get_oposite_float(number):
	number += 0.5
	if number > 1:
		number -= 1
	return number
	
func player_has_won():
	var r_difference = abs(destination_r - controled_r)
	var g_difference = abs(destination_g - controled_g)
	var b_difference = abs(destination_b - controled_b)
	
	var has_won = (
			r_difference < (INCREMENT * DIFFICULTY_TRESHOLD)
			and g_difference < (INCREMENT * DIFFICULTY_TRESHOLD)
			and b_difference < (INCREMENT * DIFFICULTY_TRESHOLD)
		)
	return has_won

func increment_points():
	points += 1
	var oposite = Color(get_oposite_float(controled_r), get_oposite_float(controled_g), get_oposite_float(controled_b),1)
	$Points.change_color(oposite, points)
