extends RichTextLabel

func change_color(color, points):
	clear()
	push_color(color)
	add_text('Points:')
	newline()
	add_text(str(points))
	pop()
