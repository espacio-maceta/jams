extends RichTextLabel

func change_color(color):
	clear()
	push_color(color)
	add_text('Match the colors!')
	newline()
	add_text('Red: +Insert, -Supr')
	newline()
	add_text('Green: +Home, -End')
	newline()
	add_text('Red: +PageUp, -PageDown')
	newline()
	add_text('Space to reset')
	pop()
